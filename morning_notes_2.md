# Morning Session 2

## Changing testing culture in ginormous companies

http://speakerdeck.com/jimholmes

Sisyphean tasks: Sharepoint!

What does a good testing culture look like?

Mortgage driven development, you need to pay the rent!

Become more technical, I cannot agree with this enough! Code review, automation, automated checks. 

Testers are here to change the system so we are not needed any more. Everyone should be testing, part of thetesting culture!

Antarctica takes a C after the r.

### Ford case study. 

Bureacracy everywhere.

Entrenched interests in the company.

Personally I prefer the Lego castle. 

Three years, one release. 250 bugs in prod, known.

Testers didn't know a single user. Testers need to know who they're advocating. 

Systemic issues can be cross scale.

Continual improvement needs to be levered into the system.

How do you find the levers? Lean: Choose your target, be impactful rather than necessarily comprehensive.

Devops in delivery teams. Definitely useful. If you can create and own your environment, you can do a lot more.

Inertia: this metaphor went off the wrong way and won't stop

How to work around the system? Find advocates. Get people to work with you to help find ways to get change!

Look for existing initiatives, if there's a process improvement initiative or learning initiative, you can work with them to get your movement multipled. These can be long levers. 

Change agents can be used as paragons or champions of testing and culture change. 

Set reasonable expectations. Change takes time, sometimes a long time. Time to change can scale with company size in a linear or worse relationship with the scale of the company.

Culture change is hard, you're fighting the business itself and people's expectations. Learn from your failures

Celebrate your wins. You need to really grab your successes and use them to push your team forward.

Filing bug reports is less efficient than finding them while they're being written and fixing them immediately. 

### Helpful things
* Learn the business
* How do you affect the mission?
* Learn about people. People skills are the hardest skills I've ever tried to learn.
* Read more books about people. Emotional intelligence2.0, Driving technical change by Prag Prog, Resistance to change is based on fear. Lean Software books. 
* You don't need to ask permission to be awesome!!

## Owning your craft

No one gets where they are by themselves.
I'd like to thank the academy and... 
Sometimes you're thrown in at the deep end, swim.
Be a good tester, because you want to be a good tester. Take pride in it.
Own it. 
Set the machine on fire. It's for the best. 
Chaos engineering, Simian army. Lots of things in AWS are not resilient. I found that out the hard way.
It feels like a lot of people don't own their systems.They really should. 
Automation is more than making checks, it's about removing repetive actions from the human flow, freeing humans to do creative things.
What passes for Chinese food here is bloody terrifying.
We are technical people, you shouldn't ask technical people, you should ask _other_ technical people. 

