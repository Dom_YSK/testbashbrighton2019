# Afternoon session 1

Practicalities of building communities at scale

Wild stallyns!
Never gonna give you up!

Influenced by Emily Webber's workshop last year. 
Testing pants.

Started with a small QA community, just 12 people. 
3 years later 50 QAs and two locations

One QA per team, but a cross team test team.
Finding topics that everyone can follow is harder as peoples expertise and interest diverge.

They lost that loving feeling. 

Nowhere to meet and talk.

The late and consistent time was excluding people who had other commitments at that time. 

Single point of failure, it was a led community, not a driven community.

Self-organising, supportive, engaging, intelligent, driven people. 

multiple locations

Was this only testers coming to micro meetings?

Testsphere, coffee roulette, lunches. 

Multiple times so everyone had an opportunity to come.

Community led. 

Film, put it online, let remote people interact as well. 

It's an octopus of community, Mentos and manatees.

How do you get people to join in? You empower them, You give them help to express themselves and get buy-in

Community can be more than just testers, invite everyone! Get some enthusiasm!

Moar snacks for people!


## Continuous Performance Testing with Eric Proegler

What happened to dinosaurs? 
What does perf testing do to help?
Risk!
If it's bad single threaded, it's going to be worse multithreaded (normally)
CPU, Network, Storage, these bottlenecks get exponentially worse as you scale. 

Concurrency issues when you get deadlocks. Very relational database? Hard to repeat

Reliability: Does this work after a million connections? Is garbage collection chunking your stack as it goes? That's going to hurt.

Concurrency testing is performance testing concurrently!

How do I react to the results. Clean up your builds. (Terraform destroy)

Make it reproducible. This is where terraform, cloudformation etc come in. 

Calibrate!

Every now and then, recalibrate to make sure you're not hiding worse code behind more efficent libraries

Guessing what a production load looks like is hard, load tests tend to be smooth.

Cloud scaling means your actual resources are extremely fluid. 

variable network conditions mean all bets are off, especially on the last mile

Unreality. 
Embrace that it's synthetic, you can't act like people and your you can try to just be evil. Remove wait times in bursts and effectively DoS yourself to reveal soak tests.

Reliability, soak tests. I should probably set some of these up for some long running server clusters. 

* Why are you performance testing? Is it just distributed profiling? 

* What is the pass/fail for a performance test? 

* What load models will you use? Why make them connected to reality?

* Reporting, What reactions does your testing cause to happen? If nothing, why are you doing it?

SRE?

# The surprising Benefits of exploring other Disciplines and industries with Connor Fitzgerald

Testing is a kind of prestidigitation?

Controlled, Conservative, Checking

I've worked there!

testing biscuits was the same as testing software

Anarchy! 
Automation will fix all the things

CCC had horrible project failures
EEE worked well
AAA worked, sort of

Does the culture of a company change how good the testing is?

So many people recommend thinking fast and slow, ubt I don't know why. It's hundreds of pages of humble bragging with 3 pages of actual useful information.

Checklist manifesto
Write a checklist for everything. You don't need to remember things. 

Blameless culture:
Report errors, no blame, use it to teach. 
Assume goodwill, identity causes not culprits, Take your time.

Culture of Questioning. 
Don't fear questioning, Biscuit driven development.
I used TDD before, Tarot driven development. 

Mnemonics, I can never remember testing ones.

The law of unintended consequences. 

Tested means checked and explored. 
Checking can be automated, exploration should _feed_ checking. Automation should support checking.

Look outside the industry for inspiration
