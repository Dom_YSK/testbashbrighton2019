#Morning session

## Cross Team Pair testing: Lessons of a Testing Traveller with Elisabeth Hocke
Concrete scope
How do we pair? Strong style
I give you the idea, you type,
Retro at the end of the session.
Take notes together.
Always feed each other ideas for testing, expand your horizons. 
The session is a starting point, you can grow from there.

Practice consciously and work from there.


## United by Security: the Test that divides us

Why do security testing?
(honestly, does anyone not do security testing?)
Risk analysis
SFdipot, should have been soft dip. But the problem with test mnemonics is that I can never remember them.

Spoofing
Tampering
Repudiation
Information disclosure
Denial of service
Elevation of privilege

Keep error messages the same when login fails.
Security training. - I did that once
Malicious user stories, I like it. As an attacker, I want to...

Automation is a force multiplier, it's not a replacement for testers.

## 
