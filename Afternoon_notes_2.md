# Afternoon sessison 2

# 99 second talk from American Express Ian

Time for the guardians to rise: machine learning 101:

Autonomous driving and auditing of these algorithms

Supervised learning:

Labelled data

Unsupervised learning:
Lots of unlabelled data

Reinforcement learning:
Work inside rules

Hello World for machine learning is handwritten digits. There's an open dataset

CNNs
Perceptons, inputs has an output based on thresholds and weights. 
Sigmoid neurons. scaled not binary
Tensor fields; N dimensional arrays
The magic leads to probability distributions

Image folding summarises the information and gives you a probability. Looks like a 6 to me, 

Without training, neural nets are worthless.

"You can't test that, it's machine learning" 

Normally you just throw unlabelled data that the system hasn't seen before. Is it right? Then deliver. Else: retrain. 
 
Sometimes we need to retrain the human I think.

Data is the lifeblood of machine learning. Labelled data is awesome. Watch out for biases. This is an American dataset, they don't seem to use the same glyphs as other countries in some digits

Malaysia has shops damn it.

Statistical sampling is the only way to work with large non-deterministic systems


## Combating biases with heuristics of diversity with Ash Coleman.

Once a tester, always a tester

Lean into discomfort
honesty
respect
no judgements
feet on the floor
open for correction

Circle of Trust:
ISA iii
IDA iii
SA  ii
DA  ii
JW  ii
AC  ii

14/30, 47%-ish

Gender bias, old one, but a good one.

There are so many biases we have inately, which do we address first? The most obvious.

Hold spaces for the unknown and express empathy for the known
Don't just support the distant exceptions you see. Support those close to you when they need it.

High standards equal low devotion.
You need to be realistic and focused

Dissent is important, agreement does not indicate unity.
Identity diversity correlates to cognitive diversity. 

* Faced with a proble take a moment to think about it. Conscious introspection.
* Give yourself 30 seconds or 3 possibilities about how it could have happend
* If necessary, or possible, ask questions
* Provide context for your reasoning
* Make decisions with confidence

